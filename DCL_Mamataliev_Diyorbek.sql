<-- Task 1
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

<-- Task 2
GRANT SELECT ON public.customer TO rentaluser;

<-- Task 3
CREATE ROLE rental;
GRANT rental TO rentaluser;

<-- Task 4
GRANT INSERT, UPDATE ON public.rental TO rental;

INSERT INTO public.rental (rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES(NOW(), 1, 1, NOW(), 1);

UPDATE public.rental  SET return_date = NOW() WHERE rental_id = 1;

<-- Task 5
REVOKE INSERT ON public.rental FROM rental;

<-- Task 6
CREATE ROLE client_TERRY_GRISSOM;

CREATE USER Terry_Johnson WITH PASSWORD 'terrygrissom';

GRANT client_TERRY_GRISSOM TO Terry_Grissom;

ALTER TABLE public.rental ENABLE ROW LEVEL SECURITY;

CREATE POLICY rental_access_policy
    ON public.rental
    FOR SELECT
    USING (
        EXISTS (
            SELECT 1
            FROM public.customer c
            WHERE c.customer_id = rental.customer_id
            AND c.first_name = 'TERRY'
            AND c.last_name = 'GRISSOOM'
            AND NOT EXISTS (
                SELECT 1
                FROM public.payment p
                WHERE p.customer_id = rental.customer_id
            )
        )
    );

ALTER TABLE public.payment ENABLE ROW LEVEL SECURITY;

CREATE POLICY payment_access_policy
    ON public.payment
    FOR SELECT
    USING (
        EXISTS (
            SELECT 1
            FROM public.customer c
            WHERE c.customer_id = payment.customer_id
            AND c.first_name = 'TERRY'
            AND c.last_name = 'GRISSOM'
            AND NOT EXISTS (
                SELECT 1
                FROM public.rental r
                WHERE r.customer_id = payment.customer_id
            )
        )
    );

